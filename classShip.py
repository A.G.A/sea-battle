'''
Класс корабль
'''

class Ship():
    """класс - корабль"""

    def __init__(self, L, direction=1):
        """
        У коробля следующие параметры:
        Длина (L) 1<=int<=4
        Координаты (XY) список координат
        """

        self.L = L
        self.deckN = L # живой корабль
        self.direction = direction # направление 1 горизонт, 2 вертикаль

    def deckdestroy(self):
        if(self.deckN > 0):
            self.deckN -= 1
